import { Component } from '@angular/core';
import { Http } from '@angular/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Randkowa';
  // wybrany user
  user = { }
  // lista users
  users = [ ]

  constructor(private http: Http) {
   this.getUsers()
  }

  getUsers(){
     this.http
      .get('http://localhost:3000/users/')
      .subscribe(response => {
        this.users = response.json()
        console.log(this.users)
      })
  }

  register(){
    this.user = {}
  }

  save(){
    // zapisujemy istniejacy profil
    if(this.user['id']){

      this.http
        .put('http://localhost:3000/users/'+this.user['id'], this.user)
        .subscribe(response => {
          this.user = response.json()
        })

    }else{
      // tworzymy nowy profil

      this.http
        .post('http://localhost:3000/users/', this.user)
        .subscribe(response => {
          this.user = response.json()
           this.getUsers()
        })
    }
  }

  photoChange($event){
    var file = $event.target.files[0];
    var reader = new FileReader();
    reader.onloadend = (e) => {
      this.user['photo'] = (reader.result)
    }
    reader.readAsDataURL(file)
  }

}
