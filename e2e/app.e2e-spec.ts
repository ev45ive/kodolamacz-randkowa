import { RandkowaPage } from './app.po';

describe('randkowa App', () => {
  let page: RandkowaPage;

  beforeEach(() => {
    page = new RandkowaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
